pipeline {   
  agent any
  environment {
    ANSIBLE_SERVER_IP = "157.245.4.215"
  }
  stages {
    stage("copy files to Ans control node/ DO droplet") {
      steps {
        script {
          // copy all files needed to SSH into EC2s and run playbook to Ans droplet
          echo "Copying files..."
          // id_rsa_no_pw is global-scoped
          sshagent(credentials:['id_rsa_no_pw']) {
            sh "scp -o StrictHostKeyChecking=no ansible/* root@$ANSIBLE_SERVER_IP:/root"
            // create tmp file w/ priv key contents
            // ansible-ec2-key is proj-scoped
            // Reference: https://www.jenkins.io/doc/book/pipeline/jenkinsfile/#for-other-credential-types
            withCredentials([sshUserPrivateKey(credentialsId: "ansible-ec2-key", keyFileVariable: 'tmpFile')]) {
                // already disabled host key checking in scp cmd above
                // using single quotes and no {} around keyFileVariable tells Groovy interpreter NOT to expose tmpFile contents on cmd line
                sh 'scp $tmpFile root@$ANSIBLE_SERVER_IP:/root/ansible.pem'
            }
          }
        }
      }
    }

    stage("Configure EC2s with Ansible") {
      environment {
        AWS_ACCESS_KEY_ID = credentials('aws_access_key_global')
        AWS_SECRET_ACCESS_KEY = credentials('aws_secret_key_global')
        AWS_REGION = "us-east-1"
      }
      steps {
        script {
          echo "Running playbook..."
          // Plugin reference: https://plugins.jenkins.io/ssh-steps/#plugin-content-pipeline-steps
          // defining droplet as a Groovy obj that takes key:val pairs
          def droplet = [:]
          droplet.name = 'ansible-control-node'
          // no $ or ${} b/c env var is not interpolated into a string
          droplet.host = ANSIBLE_SERVER_IP // or env.ANSIBLE_SERVER_IP
          // following suppresses interactive prompt
          droplet.allowAnyHosts = true
          // Jenkins global credential to SSH into droplet
          withCredentials([sshUserPrivateKey(credentialsId: 'id_rsa_no_pw', keyFileVariable: 'keyFile', usernameVariable: 'ansibleUser')]) {
            droplet.user = ansibleUser
            droplet.identityFile = keyFile
            // remote = remote node obj defined above
            // MANUALLY CONFIGURED ANS SERVER 2 WAYS:
            // 1) sshScript file in Jenkins workspace/ repo (DOES NOT SUPPORT passing args to script)
            // sshScript remote: droplet, script: "ansible-server-setup.sh"
            // 2) sshCommand to run script copied to Ans server
            sshCommand remote: droplet, command: "bash ansible-server-setup.sh $AWS_ACCESS_KEY_ID $AWS_SECRET_ACCESS_KEY $AWS_REGION"
            sshCommand remote: droplet, command: "ansible-playbook deploy-docker-generic.yaml"
          }
        }
      }
    }       
  }
}
