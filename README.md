# Jenkins CI/CD with Ansible

## Table of Contents

1. [About](#about)
2. [Requirements](#requirements)
3. [Steps](#steps)

## About

This Jenkins pipeline 1) copies an Ansible playbook and all of its configuration files to a remote dedicated server with Ansible installed (i.e. an Ansible control node), and 2) executes the playbook on the control node to configure 2 EC2 instances (i.e. Ansible managed nodes) to compose up a Docker container network running a Java application with a MySQL DB and phpmyadmin UI to view the DB.

## Requirements

To run this pipeline, you should know how to:

- manually create servers and SSH key pairs in the AWS and DigitalOcean cloud platforms.
- install plugins and create pipeline builds and credentials in Jenkins.
- run Ansible playbooks.

## Steps

1. Fork this repo.

2. Run `aws configure` locally. Provide the Access and Secret Keys of the IAM user account in which Ansible will manage resources.

#### AWS setup

3. In the configured AWS account's EC2 service, manually launch 2 EC2s (i.e. the Ansible managed nodes).

4. Create a SSH key pair Ansible will use to authenticate to the EC2s. Download and store the private key safely in your local .ssh directory.

#### DigitalOcean setup

5. Create a new DigitalOcean droplet (i.e. the Ansible control node).

6. Create a SSH key pair Ansible will use to authenticate to the droplet. Download and store the private key safely in your local .ssh directory.

#### Jenkins setup

7. To quickly spin up an EC2 instance running Jenkins in a Docker container, run [this Ansible playbook](https://gitlab.com/twndevops/ansible-exercises/-/blob/24ac350368ef8c3d7c01a21f9e572c31225b8a18/playbooks/5-run-jenkins-container-ec2-amznlinux.yaml). For instructions on how to run this playbook, see the project [README](https://gitlab.com/twndevops/ansible-exercises#playbook-5).

8. In the Jenkins UI:

- create a new pipeline for this project. Configure your fork to be the remote repository.
- install the [`sshagent`](https://plugins.jenkins.io/ssh-agent/) and [`SSH Pipeline Steps`](https://plugins.jenkins.io/ssh-steps/) plugins.
- configure (global or project-scoped) SSH private key credentials for both the droplet and EC2 private keys. For the droplet, the username should be `root`. For the EC2s, the username should be `ec2-user`.
- configure (global or project-scoped) Secret text credentials for your IAM user account's Access and Secret Keys.

#### Ansible control node configuration changes

9. In `ansible/ansible.cfg`, replace the current value with your EC2's SSH private key filename (not location).

10. In `ansible/inventory-aws_ec2.yaml`, update the region(s) in which the AWS resources should be created.

11. In `ansible/deploy-docker-generic.yaml`, under the play `Start app containers`, comment out `vars_files` and uncomment `vars_prompt`.

- KNOWN BUG: unable to securely provide the DockerHub password so that the Java app image can be pulled from the DH repository.

#### Jenkinsfile changes

12. Replace the value of `ANSIBLE_SERVER_IP` with your droplet's public IP.

13. In the stage `copy files to Ans control node/ DO droplet`, replace:

- the `sshagent` credential with the name of your Jenkins SSH private key credential for the droplet.
- the `withCredentials` credential with the name of your Jenkins SSH private key credential for the EC2s.
- `ansible.pem` with your EC2's SSH private key filename (see `ansible/ansible.cfg`).

14. In the stage `Configure EC2s with Ansible`, replace:

- `AWS_ACCESS_KEY_ID` and `AWS_ACCESS_KEY_ID` with the names of your Jenkins Secret text credentials.
- `AWS_REGION` with the region in which you launched your EC2s.
- the `withCredentials` credential with the name of your Jenkins SSH private key credential for the droplet.

#### Run the pipeline!

15. Commit the above changes and push to your remote fork.

16. From the Jenkins UI, run the build.

17. Once the build is complete, load:

- the app URL `[server_IP/hostname]:8080`. Use either EC2.
- the phpmyadmin URL `[server_IP/hostname]:8083`. Use either EC2.
