#!/usr/bin/env bash

apt update
apt install ansible -y
apt install python3-boto3 -y

# NOT COVERED IN ANSIBLE VID 24: creating .aws dir and files
mkdir .aws
# -e flag for new line to work
echo -e "[default]\n aws_access_key_id = $1\n aws_secret_access_key = $2" > credentials
mv /root/credentials /root/.aws
echo -e "[default]\n region = $3\n output = json\n aws_default_region = $3" > config
mv /root/config /root/.aws